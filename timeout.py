
import argparse # For robust command line argument parsing.
import subprocess # For robust subprocess execution.
import shlex # For constructing an appropriate argument list.

# Note: memory management can be added with the resource module, but it will take some level of research
# and it will not work for all systems or all resource usages.

# Check that the given value is positive and convert it to an integer at the same time.
def positive_int(string):
	value = int(string)
	if value < 0:
		raise argparse.ArgumentTypeError("{number} is negative")
	return value

# Define and parse command line arguments.
parser = argparse.ArgumentParser()
parser.add_argument('--command', default=[], type=shlex.split, required=False,
	help='The quote enclosed command to execute. Alternatively, commands other than --command and --timeout will fall through.', 
	metavar='CMD', dest='command'
)
parser.add_argument('--timeout', type=positive_int, required=False,
	help='A maximum amount of time, in seconds, to run the command process.', 
	metavar='T', dest='timeout'
)
# Allow unrecognized arguments to fall through as commands to be used for the final command.
#  This allows two methods of execution: giving a command string, and simply writing the command as usual,
#  but prepended with "python3 timeout.py --timeout TIME".
arguments, fallthrough_command = parser.parse_known_args()

# XOR between the two questions "are these lists empty". Only one may be.
if not (len(fallthrough_command) != 0) != (len(arguments.command) != 0):
	print("One, and only one, method of command introduction may be used at once.")
	raise SystemExit(2)

# Since one must be empty, the concatenation is whichever one isn't empty.
process = subprocess.Popen(arguments.command + fallthrough_command)
# Try to execute the program. If it fails, the exception generally indicates that time ran out.
try:
	process.communicate(timeout=arguments.timeout)
	print("The process completed successfully.")
except:
	process.kill()
	print("The process ran out of time and was killed.")