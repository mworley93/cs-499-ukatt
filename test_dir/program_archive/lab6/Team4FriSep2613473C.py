# Team 4
# Eric Prewitt, Tyler Zimmerman, Brandon Tuttle
# Section 12
# 9/26/14

import math
from graphics import *

def main():
    win = GraphWin("Draw a Triangle",500,500)
    Text(Point(250,25),"Click on 3 Points").draw(win)
    p1 = win.getMouse()
    circ1 = Circle(p1,5)
    circ1.draw(win)
    p2 = win.getMouse()
    circ2 = Circle(p2,5)
    circ2.draw(win)
    p3 = win.getMouse()
    circ3 = Circle(p3,5)
    circ3.draw(win)
    sidea = Line(p1,p2)
    sidea.draw(win)
    sideb = Line(p2,p3)
    sideb.draw(win)
    sidec = Line(p1,p3)
    sidec.draw(win)
    da = math.sqrt((p2.getX() - p1.getX())**2 + (p2.getY() - p1.getY())**2).__round__(3)
    db = math.sqrt((p3.getX() - p2.getX())**2 + (p3.getY() - p2.getY())**2).__round__(3)
    dc = math.sqrt((p3.getX() - p1.getX())**2 + (p3.getY() - p1.getY())**2).__round__(3)
    dt = (da + db +dc).__round__(3)
    s = dt//2
    A = math.sqrt(s*(s-da)*(s-db)*(s-dc)).__round__(3)
    Text(Point(400,250), "Distance 1 = " + str(da)).draw(win)
    Text(Point(400,270), "Distance 2 = " + str(db)).draw(win)
    Text(Point(400,290), "Distance 3 = " + str(dc)).draw(win)
    Text(Point(400,310), "Total Distance = " + str(dt)).draw(win)
    Text(Point(400,330), "Total Area = " + str(A)).draw(win)
    Text(Point(250,400), "Click to Close").draw(win)
    win.getMouse()
    win.close()
main()