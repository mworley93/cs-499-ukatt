# Name: Jonathon Whitmoyer
# Date: 25 Sept. 2014
# Section: 12
# answering questions regarding a Random Number Generator

from random import *

def main():
    t = 0
    for i in range(1000):  # thousand
        t += random()
    print(t/1000)

    t = 0
    for i in range(100000): # hundred thousand
        t += random()
    print(t/100000)

    t = 0
    for i in range(1000000):  #million
        t += random()
    print(t / 1000000)
    t = 0
    for i in range(100000000): # hundred million
        t += random()
    print(t / 1e8)

main()

# Q1: What can you say about the output of the two runs?
# Both runs seem to be completely random in output.
# Q2: What seed is being used in the program as it is now?
# The seed being used is the system clock.
# Q3: What can you say about the output of two runs?
# After the seed was added, the two runs were the same.
# Q4: Move the seed inside the for loop. What can you say about the output of two runs?
# It just printed a bunch of 14's.
# Q5: What can you say about the effect of a constant seed?
# If the seed is outside of a for loop, the numbes are randomly generated for that seed.
# If the seed is inside the for loop, the number is generated and repeated.
# Q6: Exactly what does the random function return?
# The random function returns a randomly generated float number.
# Q7: What is the output of the final program? What do the numbers mean> What value
# Q7: are the values approaching? Why is it that value?
# That output is a list 4 float numbers < 1. The numbers are in scientific format. The numbers are approaching the current time. It is going towards the system time because a seed was not defined and the program pulled the system time.