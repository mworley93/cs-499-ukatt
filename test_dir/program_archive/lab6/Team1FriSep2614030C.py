from graphics import*
from math import*
def main():
    win = GraphWin("Lab 6 Team",500,500)
    prompt = Text(Point(100,100),"Click on 3 points")
    prompt.draw(win)
    
    point1 = win.getMouse()
    point2 = win.getMouse()
    point3 = win.getMouse()
    
    point1x = point1.getX()
    point1y = point1.getY()
    point2x = point2.getX()
    point2y = point2.getY()
    point3x = point3.getX()
    point3y = point3.getY()    
    
    
    line1 = Line(Point(point1x,point1y),Point(point2x,point2y))
    line2 = Line(Point(point2x,point2y),Point(point3x,point3y))
    line3 = Line(Point(point3x,point3y),Point(point1x,point1y))
    
    line1.draw(win)
    line2.draw(win)
    line3.draw(win)
    
    d1 = sqrt(((point2x-point1x)**2) + ((point2y-point1y)**2))
    d2 = sqrt(((point3x-point2x)**2) + ((point3y-point2y)**2))
    d3 = sqrt(((point1x-point3x)**2) + ((point1y-point3y)**2))
    totald = str(d1+d2+d3)
    
    end = Text(Point(300,400),totald)
    d1t = Text(Point(325,425),d1)
    d2t = Text(Point(335,435),d2)
    d3t = Text(Point(345,445),d3)
    
    d1t.draw(win)
    d2t.draw(win)
    d3t.draw(win)
    totald.draw(win)
    
    getMouse()
    win.close()
    
main()
    