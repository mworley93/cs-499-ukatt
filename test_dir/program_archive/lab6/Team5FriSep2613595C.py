#Austin herman, Johnathon Whitmoyer, Brandon Jones, Jordan Johnston
from graphics import *
from math import *
def main():
    win=GraphWin("triangle", 500, 300)
    
    prompt=Text(Point(200, 50), "Click 3 points.")
    prompt.draw(win)
    
    p1= win.getMouse()
    p1.draw(win)
    c1=Circle(p1, 3)
    c1.draw(win)
    p2= win.getMouse()
    p2.draw(win)
    c2=Circle(p2, 3)
    c2.draw(win)    
    p3= win.getMouse()
    p3.draw(win)
    c3=Circle(p3, 3)
    c3.draw(win)    
    vertices= [p1,p2,p3]
    p1x=p1.getX()
    p1y=p1.getY()
    p2x=p2.getX()
    p2y=p2.getY()    
    p3x=p3.getX()
    p3y=p3.getY()    
      
    d1=sqrt(((p1x-p2x)**2)+((p1y-p2y)**2))
    d2=sqrt(((p2x-p3x)**2)+((p2y-p3y)**2))
    d3=sqrt(((p3x-p1x)**2)+((p3y-p1y)**2))
    
    g1=Text(Point(200,100), "distance 1 is "+ str(d1))
    g1.draw(win)
    g2=Text(Point(200,150), "distance 2 is "+str(d2))
    g2.draw(win)    
    g2=Text(Point(200,200), "distance 3 is "+str(d3))
    g2.draw(win)    
    
    total=d1+d2+d3
    result=Text(Point(200, 250), "Total is "+str(total))
    result.draw(win)
    
    triangle=Polygon(vertices)
    triangle.draw(win)
    
    
    win.getMouse()
    win.close()
    
main()