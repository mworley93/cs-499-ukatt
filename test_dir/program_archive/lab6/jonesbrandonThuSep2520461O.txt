Q1. I can say that the outputs of these two runs are 50 random numbers that are between values 0 and 100.


Q2.  The seed being used in the program is 0, because no values are less than zero, meaning that it is the starting value


Q3. The output of the two runs wasn't any different than it was before the seed was changed. All values were still between 0 and 100, and totally random.


Q4. All values were 14.


Q5. A constant seed on a random number generator will restrict the rgn to a single value. You would want to use a constant seed if for example: I wanted to cheat and win a board game that uses a rgn instead of dice,
 I would get my rgn to use the highest value possible so I could win. Values are approaching 0.5, because this is where a certain function is approaching when it is more towards larger values.

Q6. Program returns floating numbers that round to 0.5


Q7. Output of this program is possibly the limi of a certain function. These numbers represent values of a function as they approach really large values.






