# Zack Arnett, Skyler Martin, Blake Catron
# 09/26/2014
# Team 2
# Lab 6

from graphics import *
from math import *

def main():
    
    win = GraphWin("Triangle", 500, 500)
    
    Text0 = Text(Point(100,75), "Click 3 Points")
    Text0.draw(win)
    
    PT1 = win.getMouse()
    PT2 = win.getMouse()
    PT3 = win.getMouse()
    
    Line1 = Line(PT1, PT2)
    Line2 = Line(PT2, PT3)
    Line3 = Line(PT3, PT1)
    
    Line1.draw(win)
    Line2.draw(win)
    Line3.draw(win)
    
    Circle1 = Circle(PT1, 5)
    Circle2 = Circle(PT2, 5)
    Circle3 = Circle(PT3, 5)
    
    Circle1.draw(win)
    Circle2.draw(win)
    Circle3.draw(win)
    
    Distance1 = round(sqrt(((PT2.getX() - PT1.getX())**2) + (PT2.getY() - PT1.getY())**2),3)
    Distance2 = round(sqrt(((PT3.getX() - PT2.getX())**2) + (PT3.getY() - PT2.getY())**2),3)
    Distance3 = round(sqrt(((PT1.getX() - PT3.getX())**2) + (PT1.getY() - PT3.getY())**2),3)
    
    text1 = Text(Point(100,100), "Distance 1 " + str(Distance1))
    text1.draw(win)
    text2 = Text(Point(100,125), "Distance 2 " + str(Distance2))
    text2.draw(win)
    text3 = Text(Point(100,150), "Distance 3 " + str(Distance3))
    text3.draw(win)
    Total = Distance1 + Distance2 + Distance3 
    Text4 = Text(Point(100,175), "Total = " + str(Total))
    Text4.draw(win)
    win.getMouse()
    win.close()
    
main()