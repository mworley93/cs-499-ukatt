# Prolog
# Author:  Skyler Martin   
# Email:  skymartin55@live.com
# Section: 12
# Date: 9/1/14
'''
  Purpose: to find the area and perimeter of a right triangle, given the length
    of the two legs of the triangle
  Preconditions: (inputs)
    User supplies the lengths of the two legs as numbers
  Postconditions: (outputs)
    User greeted and prompted for input of the two legs
    Triangle area and perimeter displayed

'''
from math import sqrt # used for Pythagorean theorem

def main():
# Design and implementation

#  1.  Greet the user and identify the program
   print("Area and Perimeter of a Right Triangle")
   
#  2.  Input lengths of two legs from user
   leg1 = float(input("Enter length of first leg "))
   leg2 = float(input("Enter length of second leg "))

#  3.  Calculate the area of the triangle using product of legs divided by 2
   area = leg1 * leg2 / 2 

#  4.  Calculate the perimeter of the triangle using Pythagorean theorem
#      to find the length of the hypotenuse
   perimeter = leg1 + leg2 + sqrt(leg1**2 + leg2**2)
   
#  5. Output resulting area and perimeter
   print("The area of a right triangle with legs", leg1,"and",leg2)
   print("is", area)
   print("The perimeter of a right triangle with legs", leg1,"and",leg2)
   print("is", perimeter)

main()
# end of program file  

'''In my program I deleted the line 26, and it caused the error because the leg2 wasn't defined and could not complete the running of the program. the error message stated that leg2 was not defined.
  File "C:\Program Files (x86)\Wing IDE 101 5.0\src\debug\tserver\_sandbox.py", line 29, in main
builtins.NameError: name 'leg2' is not defined'''
