# Prolog - Lab 2 Team Assignment
# Authors:  

#Jorge Aguilera
#Tanner Wilkerson
#Venant Abohfu
#Robert Nguyen

# Emails:

#jaag223@g.uky.edu
#tanner.wilkerson@uky.edu
#vzab222@g.uky.edu
#robert.nguyen@uky.edu

# Section: 012
# Date:   9/5/14
# Purpose:
#  Program to find the volume and surface area of a sphere given the length
#       of the radius
# Preconditions: (input)
#       User supplies radius of the sphere (as a number, no error checking done)
# Postconditions:  (output)
#       User greeted with a message
#       sphere volume and surface area displayed

#### Design in numbered comments below

from math import *  # needed for value of pi

def main():
#   1. ask the user for the radius and read it in
    radius = float (input("Enter the radius of the sphere "))

#   2. calculate the volume using the formula (4/3 times pi times radius cubed)
    volume = pi * radius * radius * radius * 4/ 3  # volume of a sphere

#   3. calculate the surface area using the formula 4pi  radius squared
    surface_area = 4 * pi * radius ** 2

#   4. output the results with appropriate labels
    print ("The volume of the sphere is", volume)
    print ("The surface area of the sphere is", surface_area)

main ()
#  Program file ends here