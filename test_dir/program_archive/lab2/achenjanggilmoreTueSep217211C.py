
# 1. Place a sheet of regular copy paper down on a flat surface in front of you with the longer edges on your left and on your right
# 2. connect the top left edge to the top right edges of the paper so as to fold it hotdog style
# 3. With the paper now folded length wise in front of you, fold back one portion of the top right edge of the paper till it touches the left edge of the paper and creates a triangle
# 4. Flip the paper over
# 5. Now fold back the top left edge of the paper till it touches the right most edge of the paper and creates a triangle
# 6. Tilt the paper clockwise to the right until the triangular edge is facing right and is parallel to your face
# 7. Grab the top edge of the side of the paper closest to you and fold it downwards till it connects with the bottom most edge of the paper
# 8. Flip the paper over
# 9. Grab the top edge of the side of the paper closest to you and fold it downwards till it connects with the bottom most edge of the paper