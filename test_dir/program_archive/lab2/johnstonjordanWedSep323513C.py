# Prolog
# Author: Jordan Johnston
# Email:  jordan.johnston@uky.edu
# Section: 12
# Date: 9/3/14
'''
  Purpose: to find the area and perimeter of a right triangle, given the length
    of the two legs of the triangle
  Preconditions: (inputs)
    User supplies the lengths of the two legs as numbers
  Postconditions: (outputs)
    User greeted and prompted for input of the two legs
    Triangle area and perimeter displayed

'''
from math import sqrt # used for Pythagorean theorem

def main():
# Design and implementation

#  1.  Greet the user and identify the program
   print("Area and perimeter of a Right Triangle")

#  2.  Input lengths of two legs from user
   leg1 = float(input("Enter length of first leg "))
   leg2 = float(input("Enter length of second leg "))

#  3.  Calculate the area of the triangle using product of legs divided by 2
   area = leg1 * leg2 /2

#  4.  Calculate the perimeter of the triangle using Pythagorean theorem
#      to find the length of the hypotenuse
   perimeter = leg1 + leg2 + sqrt(leg1**2 + leg2**2)
   
#  5. Output resulting area and perimeter
   print("The area of a right triangle with legs", leg1,"and",leg2)
   print("is", area)
   print("The perimeter of a right triangle with legs", leg1,"and",leg2)
   print("is", perimeter)

main()
# end of program file
# I misspelled print in line 39 and "Traceback (most recent call last):
  # File "C:\Program Files (x86)\Wing IDE 101 5.0\src\debug\tserver\_sandbox.py", line 41, in <module>
  # File "C:\Program Files (x86)\Wing IDE 101 5.0\src\debug\tserver\_sandbox.py", line 39, in main
# builtins.NameError: name 'prnt' is not defined" came up