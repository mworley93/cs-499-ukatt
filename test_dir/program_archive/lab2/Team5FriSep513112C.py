# Prolog
# Author:  John Smith
# Email:  John.Smith@uky.edu
# Section: 00
# Date:   9/9/14
# Purpose:
#  Program to find the volume and surface area of a sphere given the length
#       of the radius
# Preconditions: (input)
#       User supplies radius of the sphere (as a number, no error checking done)
# Postconditions:  (output)
#       User greeted with a message
#       sphere volume and surface area displayed

#### Design in numbered comments below

from math import *  # needed for value of pi

def main():
#   1. ask the user for the radius and read it in
    radius = float (input("Enter the radius of the sphere "))

#   2. calculate the volume using the formula (4/3 times pi times radius cubed)
    volume = pi * radius * radius * radius * 4/ 3  # volume of a sphere

#   3. calculate the surface area using the formula 4pi  radius squared
    surface_area = 4 * pi * radius ** 2

#   4. output the results with appropriate labels
    print ("The volume of the sphere is", volume)
    print ("The surface area of the sphere is", surface_area)

main ()
#  Program file ends here


Here's a sample run for the program: 
Enter the radius of the sphere 17.2
The volume of the sphere is 21314.4411399649
The surface area of the sphere is 3717.6350825520176

