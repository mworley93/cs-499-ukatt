Skyler Martin Section-012 CS 115
1. Which is faster at accessing data, RAM or a hard drive?
RAM
2. Who was Alan Turing? Who is Turingscraft? What was the Enigma machine? 
Alan Turing- computer scientist that designed a machine to crack the Germans code in WWII.
Turingscraft- A tool used for programing instruction with interactive programming tools.
Enigma Machine- Created by Alan Turing and was used to crack the Germans code during WWII.
3. Before programs can be executed, they have to be placed where? Who gets credit for this idea? 
CPU. 
4. What does Guido van Rossum have to do with this class?
 
5. What are the components of an IDE? What is the job of each component? 
      Individual Problem #2: Computer Units 
1. Which is larger, 5000 Terabytes or 30 Gigabytes? 
5,000 Terabytes
2. If you have 25 picture files, each of which was 512 Megabytes in size, can they all fit on a memory stick with a capacity of 32 Gigabytes? If so, how much space is left over? If not, how much more space do you need?
Yes there will be 19968 Megabytes left over or 19.5 Gigabytes 
3. If you had a hard drive with a capacity of 1 Terabyte, how many movies could fit on it if a movie took up about 2 Gigabytes?  
512 movies could fit on a 1 Terabyte hard drive. 

