#How to Create a Paper Airplane
#1.Grab a Standard 8.5x11 sheet of paper.
#2.Line up the edges of the sheet in half hotdog style.
#3.Create a crease.
#4.Open up the folded piece of paper.
#5.Fold the top corners into the center (use the crease that been made to guide where the center is).
#6.With the two corners you folded in, Fold them into the center once more making an angled edge.
#7.Close the fold, by lining up the edges to and making a crease over the same original crease(from step 3&4) 
#8.Use the two flaps of the now triangle and fold them down towards the outside creating two wings.
#9.Take the Airplane and throw it! 