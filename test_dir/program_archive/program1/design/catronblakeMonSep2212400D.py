# Prolog - This is a table of gigabyte costs based on inputs of the start and end years and the formula 
# Blake Catron
# blake.catron@uky.edu
# Section 012
# Date: 9/21/14
# main function
     # step 1. display introductory message 'Big Blue Hard Drive Storage Cost'
     # step 2. input the starting year
     # step 3. input the ending year
     # step 4. input the step size for the table
     # step 5. display hard drive storage message 'Hard Drive Storage Costs Table
     # step 6. diplay start year 
     # step 7. display end year
     # step 8. display year and cost of gigabyte column headings
     # step 9. loop for the range of years given by user
     #      step 10. calculate the cost per gigabyte for the year given by the loop
     #      step 11. output year and cost per gigabyte 
     #      step 12. calculate next year by adding the step size to the year
     #      step 13. if next year is less than or equal to ending year then loop
     # step 14. end of loop
# step 15. end of program file