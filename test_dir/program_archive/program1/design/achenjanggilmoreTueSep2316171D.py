# Gilmore Achenjang
# Gilmore.achenjang@uky.edu
# Section 012
# The purpose of this program is to develope a table of costs for a range of given
# years that will show the user how the price of a gigabyte of hard drive space
# has changed over time. 
# Precondition: the begining and ending year have to be a number in integer form
# Postconditions: the table has to be outputted in two colunms in accordance with
# the assignment specifications.
#  main function
    # Step 1. Display introductory message
    # Step 2. prompt user to input starting year
    # Step 3. prompt user to input ending year
    # Step 4. prompt user to input step size for the table
    # Step 5. Display message reading "Hard Drive Storage Costs Table"
    # Step 6. Display subsequent message informing user of chosen start and 
    # end year
    # Step 7. loop for the range of years desired with step given by user
    #     Step 7.1 calculate the cost per gigabyte for the year given by the
    #     loop
    #     Step 7.2 calculate the cost per gigabyte for each subsequest year 
    #     given by the loop
    #     Step 7.3 assign a variable for each price for each corresponding year
    # Step 8. assign a variable for each year the price is calculated for 
    # Step 9. Print "Year" and "Cost per Gigabyte ($)" as two seperate columns 
    # Step 10. print each year with each coresponding price by their assigned 
    # variables
