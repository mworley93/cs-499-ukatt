#Malik Conner,Gilmore Achenjang, Akram Alghazali, Isaiah Estes
#9/24/14
#Team Design 1
#Section 012
'''
     Purpose: to produce a table of values based on a given starting year, ending year, and step size that shows the the cost per gigabyte of hard drive capacity for each year within a range that is determined by user input (by their chosen starting year, ending year, and step size)
     Preconditions: (inputs)
       User supplies starting year, ending year, and step size between years
     Postconditions: (outputs)
       User is greeted and prompted for starting year, ending year, and step size between years
       The starting year, ending year, and step size that the user input
       A table of values showing the cost per gigabyte of hard drive capacity for each year within the range is produced
       Several blank lines (for a cleaner look)
       Titles (for the table and the two columns)
   
   '''
#Step 1. Display Introductory message
#Step 2: Ask user to input starting year, ending year, and step size
#Step 3: Input starting year from user (make sure it can only be an integer)
#Step 4: Input ending year from user (make sure it can only be an integer)
#Step 5: Input step size from user (make sure it can only be an integer) 
#Step 6. Title your table "Hard Drive  Storage Costs Table"
#Step 7. Title your Year column 
#Step 8: Output a blank line
#Step 9: Output starting year
#Step 10: Output ending year
#Step 11: Output step size
#Step 12: Output a blank line
#Step 13. Title your output column "Cost Per Gigabyte ($)" 
#Step 14: Loop for the range of years desired with step given by user
        #     Step 14.1: calculate the cost per gigabyte for the year given by the loop 
        #     Cost = 10^-0.2502("user imput year"-1980)+6.304
        #     Step 14.2: Round the cost per gigabyte down to three decimal places
        #     Step 14.3: Output year and cost per gigabyte on same line (with year alligned under year column and cost per gigabyte alligned under cost per gigabyte column)   
#Step 15: Call the main function
#Step 16. Use the getMouse function to end the program by displaying "Click to end program" 


#Malik Conner
#Maco248@l.uky.edu
#Section 012
#9/22/14
#Preconditions: 
#Postcondtions: 


# supply program prolog 
#  main function
#The purpose is to find the given hardware cost per gigabyte
    # Step 1. Display introductory message
    # Step 2. The user is asked to input the starting year and ending year
    # Step 3. The user will create a table
    # Step 4. Title your table "Hard Drive  Storage Costs Table"
    # Step 5. Title your Year column 
    # Step 6. Title your output column "Cost Per Gigabyte ($)"    
    # Step 7. Loop for the range of years desired with step given by user
    # Step 8. The user will input the step size
    # Step 9. Calculate the cost per gigabyte for the year given by the loop 
    # Step 10. Use the getMouse function to end the program by displaying "Click to end program" 
    
    #Prolog
    #Author: Isaiah Estes
    #Email: isaiah.estes@uky.edu
    #Section: 12
    #Date: 9/23/14
    '''
      Purpose: to produce a table of values based on a given starting year, ending year, and step size that shows the the cost per gigabyte of hard drive capacity for each year within a range that is determined by user input (by their chosen starting year, ending year, and step size)
      Preconditions: (inputs)
        User supplies starting year, ending year, and step size between years
      Postconditions: (outputs)
        User is greeted and prompted for starting year, ending year, and step size between years
        The starting year, ending year, and step size that the user input
        A table of values showing the cost per gigabyte of hard drive capacity for each year within the range is produced
        Several blank lines (for a cleaner look)
        Titles (for the table and the two columns)
    
    '''
    #  main function
        # Step 1: Display introductory message
        # Step 2: Ask user to input starting year, ending year, and step size
        # Step 3: Input starting year from user (make sure it can only be an integer)
        # Step 4: Input ending year from user (make sure it can only be an integer)
        # Step 5: Input step size from user (make sure it can only be an integer) 
        # Step 6: Output a blank line
        # Step 7: Output title of table
        # Step 8: Output a blank line
        # Step 9: Output starting year
        # Step 10: Output ending year
        # Step 11: Output step size
        # Step 12: Output a blank line
        # Step 13: Output titles of year column and cost per gigabyte column on same line with many spaces in between them
        # Step 14: Output a blank line
        # Step 15: Loop for the range of years desired with step given by user
        #     Step 15.1: calculate the cost per gigabyte for the year given by the loop 
        #     Step 15.2: Round the cost per gigabyte down to three decimal places
        #     Step 15.3: Output year and cost per gigabyte on same line (with year alligned under year column and cost per gigabyte alligned under cost per gigabyte column) 
    #Step 16: Call the main function

    # Akram Alghazali
    # Section 12
    #  Calculate the cost of a GB during a time
    #alghazaliakram@gmail.com
        # Display introductory message
        # have given values for the cost
        # Cost = 10^-0.2502("user imput year"-1980)+6.304
        # Message pop up giving years to imput between
        # have user imput year
        # loop for the range of years desired with step given by user
        # calculate the cost per gigabyte for the year given by the loop
        # Give message giving user cost of GB
        # 
        # 
        
        # Gilmore Achenjang
        # Gilmore.achenjang@uky.edu
        # Section 012
        # The purpose of this program is to develope a table of costs for a range of given
        # years that will show the user how the price of a gigabyte of hard drive space
        # has changed over time. 
        # Precondition: any year that is a number 
        # Postconditions: the table has to be outputted in two colunms in accordance with
        # the assignment specifications.
        #  main function
            # Step 1. Display introductory message followed by a blank line 
            # Step 2. prompt user to input starting year
            # Step 3. prompt user to input ending year
            # Step 4. prompt user to input step size for the table
            # Step 5. Display message reading "Hard Drive Storage Costs Table"
            # Step 6. Display subsequent message informing user of chosen start and 
            # end year followed by a blank line
            # Step 7. Print "Year" and "Cost per Gigabyte ($)" as two seperate columns
            # Step 8. loop for the range of years desired with step given by user
            #     Step 8.1 calculate the cost per gigabyte for the year given by the
            #     loop
            #     Step 8.2 print each year with each coresponding price with the 
            #     given step size  
            # Step 9. add a blank line at end of program
        # print a blank line.. addd these in to desgin