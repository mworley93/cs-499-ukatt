#Prolog - Team Program 1

#Jorge Aguilera, Robert Nguyen, Venant Abohfu, Tanner Wilkerson

#E-mails:
#jaag223@g.uky.edu
#robertjosephn@gmail.com
#vzab222@g.uky.edu
#tanner.wilkerson@uky.edu

#CS 115 Sect. 012
#Date - 9/24/14

#Purpose: To develop a table of costs, based on the user's inputs of the starting and ending years and the formula.
#Preconditions: User inputs starting year, ending year, and step size for the table.
#Postconditions: Program outputs table with starting and ending years, as well as year values that coincide with the step size inputted by the user. Will also print the cost per gygabyte of hard drive storage based on year.

#Main Function
    #1.0  Print statement for the header of the program
    #2.0  Print \n for spacing purposes
    #3.0  Define variable for starting year based on user input
    #4.0  Define variable for ending year based on user input
    #5.0  Ask user for step size of the table based on user input
    #6.0  Print \n for spacing purposes    
    #7.0  Print title of cost table
    #8.0  Print \n for spacing purposes    
    #9.0  Print Start year so user is aware of their input
    #10.0  Print End year so user is aware of their input
    #11.0  Print \n for spacing purposes    
    #12.0  Print the categories of the table; in this case, Year and Cost Per Gigabyte
    #13.0  Print \n for spacing purposes    
    #14.0  Loop for the range of years desired with step size given by user
        #14.1  Define variable containing the function for Moore's Law that will run the loop the specified number of times
        #14.2  Print statement containing the years adjusted to the user's specifications for the step table and also the cost per gigabyte for that specific year. Print all of these under the categories of the cost table
#15.0  End of Program