# Prolog
# Author:  Venant Abohfu
# Email:  vzab222@g.uky.edu
# Section: 012
# Date: 9/23/14
#purpose:steps to calculate the storage cost per gigabyte of harddrives.
#preconditions:the formular for calculating the cost per gigabyte = 10^-0.2502(year-1980) + 6.304
#our range will be from  the year 1980 to 2010 and our step size will be 4
#design(pseudocode)
# 1)welcome to the hard drive storage cost program
# 2)choose the range of years to calculate the cost to be from 1980 to 2010
# 3)choose our step size to be 4.
# 4)we the use the loop 
#year = 1980
#for year in the range of (1980,2010,4)
# 5)the loop above gives us the years 1980,1984,1988,1992,1994,1998,2002 and 2006 
# to which we will use to calculate the storage cost
# 6)from the list of years above, we use 1980 as our lower boundary and 2006 as our
#upper boundary.
# 7)we calculate the storage cost for the lower boundary case 1980 using the 
#formular cost per gigabyte = 10^-0.2502(year-1980) + 6.304
# 8)we calculate the storage cost for the year off by one the lower boundary.This 
#will be the year 1979
# 9)calculate the storage cost for the year more by one the lower boundary.This 
#will be the year 1981
# 10)we then calculate the storage cost for 1984,1988,1992,1994,1998 and 2002 being 
# those for our normal cases
# 11)we calculate the storage cost for the upper boundary case 2006 
# 12)calculate the storage cost for the year off by one the upper boundary.This 
#will be the year 2005
# 13)calculate the storage cost for the year more by one the upper boundary.This 
#will be the year 2007
# 14)finally, calculate the cost for a string like "james" using the formular and 
#see how it behaves.