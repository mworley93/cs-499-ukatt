#Evan Wilson
#Section 012
#evan.wilson@uky.edu
#Program 1 Design

#Make sure math module is imported
# main function
    # Step 1. Display intro message using print()
    # Step 2. Get start/end year and step size and have those values assigned to unique variables
    # Step 3. Define cost variable for use in for loop, setting it equal to 0
    # Step 4. Display table titles using print() and the variables assigned by the user's input
    # Step 5. Use a for loop with the values assigned to the variables using the range method.
    # In for loop, reassign cost variable with equation given for cost of storage and i variable incremented by for loop - which will be determined by the user input for the start, end, and step size variables
    # Still in the for loop, print i variable which will print the year the loop is currently on and the cost which will be rounded using round method
    