# Jonathon Whitmoyer
# Section 12
# jdwh229@g.uky.edu
# 21 Sept. 2014
# The purpose of this program is to create a table displaying the approximate cost of hard drive space with the coinciding year
# Input a starting year and an ending year along with a step size variable.
# The program will produce a table that will display a year and cost per gb of storage for the year used.

# import

# main function
    # 1. Display the introductory message.
    # 2. have the program take user input for calculations and display
    # 3. print the remaining functions for formatting
    # 4. begin a for loop using the inputs the user was asked for
        # 5. define the function that is used ton calculate price per year
        # 6. finish formatting
# 7. call main function