# Skyler Martin, skymartin55@live.com   
# Section 012
# To calculate the cost per gigabyte for the years inputed by the user
# 9/21/14
# Preconditions: inputs from the user to get the year range, and step size.
# Postconditions: cost for a gigabyte for the given year and the loop is ran for each year in step size.

#main function
    # 1. Display a message greeting the user.
    # 2. Ask the user for the beinning year.
    # 3. Ask the  user for the ending year.
    # 4. Ask the user for the step size.
    # 5. Use loop to calculate each cost by year
         # 5.1 Find cost for starting year.
         # 5.2 Find cost for each year in between in step size.
         # 5.3 Find cost for ending year.
    # 6. Display a title for the table.
    # 7. State the starting year.
    # 8. State the ending year.
    # 9. Display column labeled Year.
         # 9.1 Create and Display table using starting and ending year, along with step size.
    # 10. Display column labeled Cost.
         # 10.1 Create and Display table using loop for cost for each year.
    # 11. Say "Good Bye"