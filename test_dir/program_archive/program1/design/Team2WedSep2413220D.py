# Blake Catron, Evan Wilson, Zack Arnett, Skyler Martin 
# Section 012
# Team 2
# 9-24-14
# main function
     # step 1. display introductory message 'Big Blue Hard Drive Storage Cost'
     # step 2. input the starting year
     # step 3. input the ending year
     # step 4. input the step size for the table
     # step 5. display hard drive storage message 'Hard Drive Storage Costs Table
     # step 6. diplay start year 
     # step 7. display end year
     # step 8. display year and cost of gigabyte column headings
     # step 9. add formatting
     # step 10. loop for the range of years given by user
     #      step 11. calculate the cost per gigabyte for the year given by the loop
     #      step 12. output year and cost per gigabyte 
     # step 13. end of loop
# step 14. end of program file