# Brandon Tuttle
# brandon.tuttle@uky.edu
# Section 012
# 9/22/14
# The purpose of this program is to take a range of years as inputs from the user and calculate the cost per gigabyte of storage for each of the years in said range.
# The program will receive a list of years as input from the user and input each of the years in that list into the equation 10^(-0.2502(year-1980) + 6.304) to compute the cost per gigabyte of storage during that year.
# The outputs of this program should be a table containing the years in the range selected by the user and the cost per gigabyte associated with each particular year within that range.
# Step 1. First, I would include a print statement describing what this program does to the user.
# Step 2. Get things started by naming the program e.g. "Hard Drive Storage Cost Over the Ages."
# Step 3. Get a starting year from the user using an input statement.
# Step 4. Get an ending year from the user using yet another input statement.
# Step 5. Ask the user what step size they want for the table, e.g. 1, 2, 3
# Step 6. Print a label for the table of cost comparisons.
# Step 7. Create a loop for the range of years given by the user.
# Step 8. Use the equation given at the outset of this program to calculate the cost per gigabyte of storage for the range in the loop.
# Step 9. Using the print function, print the given year and it's calculation given by the equation.
# Step 10. Tinker with the print function to ensure that it prints each year and calculation on a new line.
# Step 11. Tinker with the output of the print function using tab characters to make the table nice and neat and actually look like a table.