#Prolog
#Author: Isaiah Estes
#Email: isaiah.estes@uky.edu
#Section: 12
#Date: 9/23/14
'''
  Purpose: to produce a table of values based on a given starting year, ending year, and step size that shows the the cost per gigabyte of hard drive capacity for each year within a range that is determined by user input (by their chosen starting year, ending year, and step size)
  Preconditions: (inputs)
    User supplies starting year, ending year, and step size between years
  Postconditions: (outputs)
    User is greeted and prompted for starting year, ending year, and step size between years
    The starting year, ending year, and step size that the user input
    A table of values showing the cost per gigabyte of hard drive capacity for each year within the range is produced
    Several blank lines (for a cleaner look)
    Titles (for the table and the two columns)

'''
#  main function
    # Step 1: Display introductory message
    # Step 2: Ask user to input starting year, ending year, and step size
    # Step 3: Input starting year from user (make sure it can only be an integer)
    # Step 4: Input ending year from user (make sure it can only be an integer)
    # Step 5: Input step size from user (make sure it can only be an integer) 
    # Step 6: Output a blank line
    # Step 7: Output title of table
    # Step 8: Output a blank line
    # Step 9: Output starting year
    # Step 10: Output ending year
    # Step 11: Output step size
    # Step 12: Output a blank line
    # Step 13: Output titles of year column and cost per gigabyte column on same line with many spaces in between them
    # Step 14: Output a blank line
    # Step 15: loop for the range of years desired with step given by user
    #     Step 15.1: calculate the cost per gigabyte for the year given by the loop 
    #     Step 15.2: Round the cost per gigabyte down to three decimal places
    #     Step 15.3: Output year and cost per gigabyte on same line (with year alligned under year column and cost per gigabyte alligned under cost per gigabyte column) 
#Step 16: Call the main function