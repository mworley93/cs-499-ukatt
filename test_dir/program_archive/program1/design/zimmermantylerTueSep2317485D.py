#Tyler Zimmerman
#CS 115, Section 012
#9/23/'14

#def main():
    #Step 1. Display introductory message.
    #Step 2. Ask user what 2 years they would like the loop to contain.
    #Step 3. Tell the user that a negative step requires a higher year first,
        #and a lower year second, and vise versa.
    #Step 4. Ask user what size step in years they would like.
    #Step 5. Have the loop contain the first year the user provided
        #and the second year the user provided +1.
    #Step 6. Have the loop step with the number the user inputed.
    #Step 7. Calculate the cost per GB during the years the user provided
        #Using the appropriate formula.
    #Step 8. Print the cost per GB for the years the user requested.
    #Step 9. Tell the user they can restart the program if they wish to
        #learn different prices.
    #Step 10. Shut down when the user exits out.
