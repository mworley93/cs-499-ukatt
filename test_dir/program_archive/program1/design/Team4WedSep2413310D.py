# Brandon Tuttle, Eric Prewitt, Tyler Zimmerman
# Section 12, 9/24/14
# The purpose of this program is to take a range of years as inputs from the user and calculate the cost per gigabyte of storage for each of the years in said range.
# The program will receive a list of years as input from the user and input each of the years in that list into the equation 10^(-0.2502(year-1980) + 6.304) to compute the cost per gigabyte of storage during that year.
# The outputs of this program should be a table containing the years in the range selected by the user and the cost per gigabyte associated with each particular year within that range.
# Step 1. Print a statement describing what this program does to the user.
# Step 2. Print a header for the program e.g. "Hard Drive Storage Cost Over the Ages." spaced on each side.
# Step 3. Get a starting year from the user using an input statement.
# Step 4. Get an ending year from the user using yet another input statement.
# Step 5. Ask the user what step size they want for the table, e.g. 1, 2, 3
# Step 6. Print start and end year on seperate lines.
# Step 7. Print collumn labels for the table of cost comparisons.
# Step 8. Loop for the range of years given by the user.
    # Step 9. Use the equation given at the outset of this program to calculate the cost per gigabyte of storage for the range in the loop.
    # Step 10. Using the print function, print the given year and it's calculation given by the equation seperated by tabs and on their own lines (as to create a table)