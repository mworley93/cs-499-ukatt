# Zack Arnett
# Section 012
# 09/23/2014
# Program 1


#  main function
def main():
    
    # Step 1. Display introductory message
    
    print("Big Blue Hard Drive Storage Cost")
    
    print("   ")
    
    # Step 2. Print the input function for The starting year
    
    Start = int(input("Enter the starting year: "))
    
    # Step 3. Print the input function for the ending year
    
    End = int(input("Enter the Ending Year: "))
    
    # Step 4. Print the input function for the Step size for the Table
    
    Step = int(input("What Step Size for the Table: "))
    
    print("    ")
    
    # Step 5. Display sub Heading
    
    print("      Hard Drive Storage Costs Table")
    
    print("    ")
    
    # Step 10. Display years in One column
    
    print("Start Year = ", Start)
            
    # Step 11. Display cost parallel to years
    
    print("End Year = ", End)
    
    print("    ")
    
    print("    ", "Year", "           ", "Cost Per Gigabyte ($)")
    
    print("    ")    
      
    # Step 8. Loop for the range of years desired with step given by user
    
    for i in range(Start, End + 1, Step):
    
        # Step 9. Calculate the cost per gigabyte for the year given by the loop
        
        CPG = round((10**((-0.2502*(i-1980)) + 6.304)), 3)
    
        # Step 12.
        
        print("    ", i, "           ", CPG)
    
main()


# The equation can never mathematically equal zero. It can only approach closer and closer to zero. But in the program the Cost per
# Gigabyte only rounds to 3 decimal places. So at the Year 2019 the program runs it as zero because there is no number before the 
# third decimal place. I got this by simply running a test from 2000 to 3000 with a step of 1. This shows me that 2018 is 0.001 and 
# 2019 is 0.0. 