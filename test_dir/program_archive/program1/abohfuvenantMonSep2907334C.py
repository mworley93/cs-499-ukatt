# Prolog
# Author:  Venant Abohfu
# Email:  vzab222@g.uky.edu
# Section: 012
# Date: 9/28/14
#purpose:steps to calculate the storage cost per gigabyte of harddrives.
#preconditions:the formular for calculating the cost per gigabyte = 10^-0.2502(year-1980) + 6.304
#our range will be from  the year 1980 to 2010 and our step size will be 4
#postconditions:output the cost per Gigabyte($) for years in the range 1980 to 2010 with a step size of 4
#Source code
def main():
    # Step 1. Display introductory message
    print("Welcome to the Amazing Hard Drive Storage Cost")

       # Step 2. Prompt input start year
    year1 = int(input("Enter the starting year: "))
    
       # Step 3. Prompt input end year
    year2 = int(input("Enter the ending year: "))
    
       # Step 4. Prompt input step
    step = int(input("Enter the step size: "))
    
    # Step 5. print "Hard Drive Storage Costs Table"
    print("Hard Drive Storage Costs Table")
    
     # Step 6. print start and end year
    print("Start Year = 1980")
    print("End Year = 2010")
    
    # Step 7. print column headers "year" and "cost per gig"
    print("year", "cost per Gigabyte($)",sep = "    ")
    
       # Step 8. loop for the range of years desired with step given by user
    for year in range(1980,2010,4):
        
       # Step 9. calculate the cost per gigabyte for the year given by equation and loop
        
        costpergigabyte =(10)**(-0.2502*(year-1980) + 6.304)
        

        
       # Step 10. print year and its correponding cost from the loop into columns
        
        print(year,  round(costpergigabyte,3),sep = '      ')
main()