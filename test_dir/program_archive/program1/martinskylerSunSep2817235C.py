# Prolong
# Skyler Martin, skymartin55@live.com   
# Section 012
# To calculate the cost per gigabyte for the years inputed by the user
# 9/21/14
# Preconditions: inputs from the user to get the year range, and step size. the year being gretaer than 1980
# Postconditions: cost for a gigabyte for the given year and the loop is ran for each year in step size.

#main function

def main():
    
# 1. Display a message greeting the user.
    
    print("Big Blue Hard Drive Storage Cost")
    
    # 1.1 Print space line
    
    print("")
    
# 2. Ask the user for the beinning year.

    startyear = int(input("Enter the starting year: "))
    
# 3. Ask the  user for the ending year.

    endyear = int(input("Enter the ending year: "))
    
# 4. Ask the user for the step size.

    stepsize = int(input("What step size for the table? "))
    
    # 4.1 Print space line
    
    print(" ")
    
    
# 5. Display a title for the table.

    print("        Hard Drive Storage Costs Table")
    
    # 5.1 Print space line
    print(" ")
    
# 6. State the starting year.

    print("Start year = ", startyear)
    
# 7. State the ending year.

    print("End year = ", endyear)
    
    #7.1 Print space line
    
    print(" ")
    
# 9. Display column labeled Year, and colum labeled Cost

    print("   Year","   Cost Per Gigabyte ($)",  sep ='        ')
    
    # 9.1 Print space line
    
    print("")
    
    
# 10. Use the for loop to calculate cost by year

    for i in range(startyear, endyear + 1, stepsize):
        cost = 10 ** (-0.2502 * ( i - 1980) + 6.304)
        cost = round(cost, 3)
        print('  ',  i,'             ',cost)
        
        
        
    
main()

# Bouns
    # The expotential function will be zero during the year 2401. But because we are using the round function in the source it will occur in 2019.  