# Gilmore Achenjang

# Section 012

# Gilmore.achenjang@uky.edu

# September 25, 2014

# The purpose of this program is to develope a table of costs for a range of given
# years that will show the user how the price of a gigabyte of hard drive space
# has changed over time. 

# Precondition: Any year that is a number.

# Postconditions: The table outputs the year specified by the user and the 
# corresponding price for a gigabyte of hard drvie for that year as predicted by
# a formula.

#  main function

def main():

    # initial empty space for formatting
    
    print()
    
    # Step 1. Display introductory message followed by a blank line 
    
    print("Big Blue Hard Drive Storage Cost")
 
    print()
    
    # Step 2. prompt user to input starting year
    
    startyr = int(input("Enter the starting year: "))
    
    # Step 3. prompt user to input ending year
    
    endyr = int(input("Enter the ending year: "))
    
    # Step 4. prompt user to input step size for the table follwed by a space

    stepsize = int(input("What step size for the table? "))

    print()

    # Step 5. Display message reading "Hard Drive Storage Costs Table" followed 
    # by a space
    
    print("        Hard Drive Storage Costs Table")

    print()
    
    # Step 6. Display subsequent message informing user of chosen start and 
    # end year followed by a blank line
    
    print ("Start Year = ", startyr)
    
    print("End Year = ", endyr)

    print()
           
    # Step 7. Print "Year" and "Cost per Gigabyte ($)" as two seperate columns 
    # followed by an empty space
    
    print("   Year", "          ","Cost Per Gigabyte ($)")
    
    print()
    
    # Step 8. loop for the range of years desired with step given by user
    
    for i in range(startyr, endyr + 1, stepsize):
        
    #     Step 8.1 calculate the cost per gigabyte for the year given by the 
    #     loop
        
        price = 10** (-0.2502 * (i-1980) + 6.304)
        
    #     Step 8.2 print each year with each coresponding price with the 
    #     given step size 
        
        print("  ",i,"                 ",round(price,3))       
    
    # Step 9. add a blank line at end of program
    
    print()
    
main()

# Bonus: the cost formula does eventually reach a cost of zero. The first year
# to cause it to display a zero value is the year 2019. 

