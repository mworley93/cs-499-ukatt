#Blake Catron
#CS 115
#Section 012
#This program tells how much a gigabyte costs each year from the years 1980-2010
def main():
    print("Big Blue Hard Drive Storage Cost")
    syear = int(input("Enter the starting year: "))
    eyear = int(input("Enter the ending year: "))
    step_size = int(input("What step size for the table? "))
    stop_year = int((eyear-syear)/step_size+1)
    print("          Hard Drive Storage Costs Table")
    print(" ")
    print("Start Year = ",syear)
    print("End Year = ",eyear)
    print(" ")
    print("   Year           Cost Per Gigabyte ($)")
    print(" ")
    for i in range(stop_year):
        CPG = 10**(-0.2502*(syear-1980)+6.304)
        print("  ",syear,"           ",format(CPG, '.3f'))
        syear = syear + step_size
main()