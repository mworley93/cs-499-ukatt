#Brandon Jones
#Section12
#bdjo232@g.uky.edu
#9/28/14
#Purpose: to acclomate all learned skills so far into one program.


def main():
    #Print the introductary phrase by writing a simple print statement
    print('Big Blue Hard Drive Storage Cost')
    #Use print statement to space a line between the introductary statements
    print("")
    
    
    #Set up an integer input for the starting year, name it year1
    year1 = int(input('Enter the starting year: '))
    #Set up an integer input for the ending year, name it year2
    year2 = int(input('Enter the ending year:  '))
    #Set up an integer input for the step size, name it step
    step = int(input('What step size for the table?  '))
    #Use print statement to space a line
    print("")
    #To print a heading that is centered use the print statement with two seperate phrases, one being simply spaces so that the heading will center
    
    print("      ",'Hard Drive Storage Costs Table')
    #Use a print statement to space a line
    
    print("")
    #Print the integer inputs from earlier in code
    print(('Start Year = '), year1)
    print(('End Year ='), year2)
    #Use print statement to space a line
    
    print("")
    #Use a print statement to set up two different columns
    print("Year                     Cost Per Gigabyte ($)")
    #Use a print statement to space a line
    print("")
    
    #set up for loop, with year1 being starting year, year2 being ending year, and step being interval
    for i in range (year1, (year2 + (1)), step):
        #set up formula and plug in variables at appropriate places
        cost = round((10**((-.2502)*((i)-1980) + 6.304)), 3)
        #print each variable in the corresponding columns by using a spacer
        print(year1, "                           ", cost)
        #set year1 equal to year1 + step to proceed with the list made
        year1 = ((year1) + (step))
        
    
 #End the function
    
main()
    
    #The number/year 2045 will make the function equivalent to 0.