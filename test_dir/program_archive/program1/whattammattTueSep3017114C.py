#Matt Whattam
#CS 115 section 012
#9/30/14

from math import*
def main():
    print("Big Blue Hard Drive Storage Cost")
    print("\n")
    startingYear = int(input("Enter the starting year: "))
    #user inputs beginning year for range
    endingYear = int(input("Enter the ending year: "))
    #user inputs end year for range
    stepSize = int(input("What step size for the table? "))
    #user inputs number years to be skipped
    print("\n")
    print("\t" "Hard Drive Storage Costs Table")
    print("\n")
    print("Start Year =",startingYear)
    print("End Year =",endingYear)
    print("\n")
    print("   Year" "\t" "\t" "Cost Per Gigabyte ($)")
    print("\n")

    for i in range(startingYear,endingYear+1,stepSize):
        cost = 10**(-0.2502*(i-1980)+6.304)
        print("  ",i, "\t""      ",cost.__round__(3))
        #this is the equation that calcultes cost per gigabyte within the range
main()
