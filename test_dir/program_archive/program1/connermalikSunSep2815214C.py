#Malik Conner
#Maco248@l.uky.edu
#Section 012
#9/27/14
#Preconditions: User's integer inputs for start year, ending year, and step size
#Postcondtions: Using the round function for our output, Using the loop function, Calculating Cost per GB


# supply program prolog  
#  main function
#The purpose is to find the given hardware cost per gigabyte
    # Step 1. Display introductory message
    # Step 2. The user is asked to input the starting year and ending year
    # Step 3. The user will create a table
    # Step 4. Title your table "Hard Drive  Storage Costs Table"
    # Step 5. Title your Year column 
    # Step 6. Title your output column "Cost Per Gigabyte ($)"    
    # Step 7. Loop for the range of years desired with step given by user
    # Step 8. The user will input the step size
    # Step 9. Calculate the cost per gigabyte for the year given by the loop 
    # Step 10. Use the getMouse function to end the program by displaying "Click to end program" 
    
#Here we're importing our math function
from math import *
def main():
#This displays our title for the overall program
    print("Big Blue Hard Drive Storage Cost")
    print()
    
#These will ask for the user's inputs
    start= int(input("Enter starting year:"))
    end= int(input("Enter ending year:"))
    skip= int(input("Enter step size for the table:"))
    print()
    
#Here is where we'll display the title for the table
    print("\t Hard Drive Storage Costs Table")
    print()
    
#This displays the user's start and ending year
    print("Start Year:", start)
    print("End Year:", end)
    print()
    
#This displays the calculations for the year and cost based on the user's inputs
    print("Year \t Cost Per Gigabyte ($)")
    print()
    
#Here is where we begin our loop 
    for i in range(start,end+1,skip):
        
#This is our equation used to calculate the cost per GB
#We use the round function to round the third decimal place
        num=round(10**((-0.2502*(i-1980))+6.304),3)
        
 #This will display the outputs based on the equation and the user's inputs
 #It will also display a list of outputs between the user's start year, ending year, and step size
        print(i,"\t" "\t", num)
   
main()

#The year 2019 will mathematically give you an output of zero