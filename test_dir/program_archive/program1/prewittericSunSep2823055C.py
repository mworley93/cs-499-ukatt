# Cost Per Gigabyte by year calculator, uses formula to calculate hard drive storage cost per gigabyte for given years
# CS 115 Fall 2014 Program 1, 9/28/14
# Eric Prewitt, Section 012, erpr222@g.uky.edu
# Preconditions: Receives starting year, ending year and a step size for the table
# Postconditions: Outputs cost per gigabyte for given years along

def main():
    
    # Display introductory message
    print("Big Blue Hard Drive Storage Cost")
    
    # Ask user for lower and higher end of year range and assign to variables
    start = input("\nEnter the starting year: ")
    end = input("Enter the ending year: ")
    # Ask user for step size desired and assign to variable
    step = input("What step size for the table? ")
    
    #  Print table "header" (name, start/end years, column identifiers)
    print("\n\t Hard Drive Storage Costs Table")
    print("\nStart year = " + str(start))
    print("End Year = " + str(end))
    print("\n  Year \t\tCost Per Gigabyte ($)\n")
    
    # For loop with range and step size given by the earlier inputs, one added to end year as to calculate cost for end year also
    for i in range(int(start), int(end)+1, int(step)):
        # calculate the cost per gigabyte for the year given by the loop
        cost = 10**(-0.2502*(i-1980)+6.304)
        roundcost = round(cost, 3)
        # Print year and cost per gigabyte 
        print("  "+str(i)+"\t\t"+str(roundcost))
    
main()

# 2019 is the first year to display a zero for the cost.