#Brandon Tuttle
#Section 012
#9/28/14
#This program is designed to take two years given by the user to create a range of years that are entered into a formula inside of a for loop to create a table of the cost per gigabyte of storage over the chosen range of years.
def main():
    print("\t\tCost of Hard Drive Storage Across The Ages") #Title of the program.
    print('') #Spacing.
    x = int(input("Enter a starting year: ")) #Asking for the starting year from the user and converting it into an integer.
    y = int(input("Enter an ending year: ")) #Asking for the ending year from the user and converting it into an integer.
    z = int(input("Enter a step size for the table: ")) #Asking for the step size of the table/range of years.
    print('') #Spacing.
    print("\tHard Drive Storage Costs Per Year") #Title of the table.
    print('') #Spacing.
    print("\tStarting year: " + str(x)) #Printing the starting year for the user.
    print("\tEnding year: " + str(y)) #Printing the ending year for the user.
    print('') #Spacing.
    print("\tYear\t\tCost Per Gigabyte ($)") #Labels for the columns of the table.
    for i in range(x,y+1,z): #The for loop, taking the inputs given by the user earlier to create a range and a step size for that range.
        cost = 10 ** (-0.2502 * (i - 1980) + 6.304) #The formula for calculating the cost per gigabyte of storage.
        print('\t', i ,'\t\t', cost.__round__(3)) #The print statement for the values of the table.
main()
#The exponential function approaches zero as the argument of the exponential approaches negative infinity. So, technically, it never truly reaches zero. However, for our purposes, an approximation should do. So, the answer really depends on how close to zero you would like to go. Generally, I would say that if the order of magnitude is greater than -10, then that should be more than close enough to zero. So, the first year where the calculation reaches an order of magnitude greater than -10 is 2085.