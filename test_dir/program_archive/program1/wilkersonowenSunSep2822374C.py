#Program 1 Source  
#Owen Tanner Wilkerson
#tanner.wilkerson@uky.edu
#Section 12
#9/28/14
#purpose: To create a table of the cost per gigabytes according to the year
#Preconditions: user inputs start year, stop year, and the step size for the table
#postconditions: program outputs a table with the starting and ending years, as well as the cost per gigabyte that go with the year.

def main():
    #title
    print("Big Blue Hard Drive Storage Cost")
    
    #input prompts for start year, end year and step
    startYear=int(input("Enter the starting year? "))
    endYear=int(input("Enter the ending year? "))
    step=int(input("What step size for the table? "))
    
    #column header print statement
    print("Year", "Cost Per Gigabyte($)", sep='    ')
    
    #loop function that runs the inputed year through the calculations
    for i in range(startYear, endYear + 1, step):
        
        #broke the equation into pieces to avoid mistakes
        year=(i) - 1980
        multiplyYear=year * -0.2502
        add=multiplyYear + 6.304
        exponent=10 ** add
        
        #round to 3 decimal places
        solution=round(exponent,3)
        
        #print out the results
        print(i, solution, sep='    ')
main()
#year 2019 is the first year that displays $0 as the cost