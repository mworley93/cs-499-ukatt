#Akram Alghazali
#Section 12
#Calculate cost of GB for a set of years
#alghazaliakram@gmail.com
#cost is 10**(-0.2502(year-1980) +6.304)
def main():
    #print program name
    print("Hard Drive Storage Cost")
    #adds space inbetween line prints
    print("\n")
    year1 = int(input("Enter the starting year: "))
    #user input of first year
    endyear = int(input("Enter the ending year: "))
    #user input of end year
    step = int(input("What is the step size for the table?: "))
    #user input of steps
    #start of table for the cost and years user imputs
    print("\n")
    print("\t Hard Drive Storage Cost Table")
    print("\n")
    print("Start year = ", year1)
    print("End year = ", endyear)
    print("\t" "Year" "\t \t" "cost")
    #loop for calculation of first year and the steps user want
    for i in range(year1, endyear+1, step):
        cost = 10**(-0.2502*(year1-1980)+6.304)
        print("\t", year1, "\t \t", cost)
        year1 = year1 + step
        
        
main()