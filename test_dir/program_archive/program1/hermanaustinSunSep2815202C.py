#Austin Herman, Section 12, 9/28/14
#Email:ashe223@g.uky.edu
#This program is based off of the decay in price per gigabyte with respect to time.
#Enter in a starting year, ending year, and step size, and the program will show a table of costs for each year

def main ():
	#Title
	print("\tThe decay of cost per gigabyte each year.\n")
	#Ask user for inputs.
	year1=int(input("Enter the starting year:"))
	year2=int(input("Enter the ending year:"))
	step=int(input("Enter the step size for the table:"))
	#Result title.
	print("\n\tHard Drive storage cost results. \n")
	#Reciting user inputs.
	print("Starting year is ",year1)
	print("Ending year is ", year2)
	#Table title
	print("\n\t Year \tCost Per Gigabyte ($)\n")
	#Result loop formatted into a table.
	for i in range(year1, year2+1, step):
		cpg=10**((-.2502*(i-1980))+6.304)
		print("\t",round(i,3),"\t", round(cpg,3))
main()