# Jonathon Whitmoyer
# Section 12
# jdwh229@g.uky.edu
# 21 Sept. 2014
# The purpose of this program is to create a table displaying the approximate cost of hard drive space with the coinciding year
# Input a starting year and an ending year along with a step size variable.
# The program will produce a table that will display a year and cost per gb of storage for the year used.

from math import *
from decimal import *

def main():
    print(" ")
    print("Big Blue Hard Drive Storage Cost",)
    print(" ")
    year = int(input("Enter the starting year: "))
    year1 = int(input("Enter the ending year: "))
    size = int(input("What step size for the table? "))
    print(" ")
    print("        Hard Drive Storage Costs Table")
    print(" ")
    print("Start year =",year)
    print("End Year =",year1)
    print(" ")
    print("Year        Cost Per Gigabyte ($)")
    print(" ")
    for i in range(year,year1,size):    
        cost = 10 ** (-.2502 * (i-1980) +6.304)
        s = format(cost, '.3f')
        print(i, s, sep='\t\t')
main()