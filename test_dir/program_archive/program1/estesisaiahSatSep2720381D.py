#Prolog
#Author: Isaiah Estes
#Email: isaiah.estes@uky.edu
#Section: 12
#Date: 9/26/14
'''
  Purpose: to produce a table of values based on a given starting year, ending year, and step size that shows the the cost per gigabyte of hard drive capacity for each year within a range that is determined by user input (by their chosen starting year, ending year, and step size)
  Preconditions: (inputs)
    User supplies starting year, ending year, and step size between years
    User supplies their name
  Postconditions: (outputs)
    User is greeted and prompted for starting year, ending year, and step size between years
    User's name is displayed in greeting and name of table
    The starting year, ending year, and step size that the user input
    A table of values showing the cost per gigabyte of hard drive capacity for each year within the range is produced
    Several blank lines (for a cleaner look)
    Titles (for the table and the two columns)

'''
#  main function

    
    # Step 1: Display introductory message and ask user's name
    
    
    # Step 2: Print user's name
    
    
    # Step 3: Ask user to input starting year, ending year, and step size
    
    
    # Step 3: Input starting year from user (make sure it can only be an integer)
    
    
    # Step 5: Input ending year from user (make sure it can only be an integer)
    
    
    # Step 6: Input step size from user (make sure it can only be an integer)
    
    
    # Step 7: Print a blank line
    
    
    # Step 8: Print title of table
    
    
    # Step 9: Print a blank line
    
    
    # Step 10: Print starting year
    
    
    # Step 11: Print ending year
    
    
    # Step 12: Print step size
    
    
    # Step 13: Print a blank line
    
    
    # Step 14: Print titles of year column and cost per gigabyte column on same line with many spaces in between them
    
    
    # Step 15: Print a blank line
    
    
    # Step 16: Loop for the range of years desired with step given by user

        
    #     Step 16.1: calculate the cost per gigabyte for the year given by the loop 
        
        
    #     Step 16.2: Round the cost per gigabyte down to three decimal places
        
        
    #     Step 16.3: Print year and cost per gigabyte on same line (with year alligned under year column and cost per gigabyte alligned under cost per gigabyte column)       

#Step 17: Call the main function
