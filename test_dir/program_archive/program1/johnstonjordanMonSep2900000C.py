# Jordan Johnston
# jordan.johnston@uky.edu
# section 012
# 9/28/14
# program one, calculates the cost of gigabytes in a given year and compares them through a for loop

#  main function
from math import *

def main():
    
    # Step 1. Display introductory message
    print("Big Blue Hard Drive Storage Cost")
    
    # Step 2. Display input messages for starting and ending year
    stryear= int(input("Enter the starting year: "))
    endyear= int(input("Enter the ending year: "))
            
    # Step 3. Display input message for step size
    
    stp= int(input("What step size for the table: "))
            
    # Step 4. Display message with the name of table
    print("        Hard Drive Storage Costs Table")
    
    # Step 5. Display message with the start and end year
    print("Starting year= ", stryear)
    print("Ending year= ", endyear)
    
    # Step 6. Display message that displays the name of year column 
    # Step 7. Display message that displays the name of cost per gig column
    print("Year          Cost Per Gigabyte ($)")
    # Step8: loop for the range of years desired with step given by user
    for i in range(stryear, endyear+1, stp):
        #     Step 9. calculate the cost per gigabyte for the year given by the loop
        year= i-1980
        x1= -0.2502 * year
        x2= x1+6.304
        x3= 10**x2
        # Step 10. display results from the calculations
        print(i,"         ", round(x3, 3))
    print()    
    # Step 11. end main
main()