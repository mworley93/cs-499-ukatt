#Prolog - Program 1
#Jorge Aguilera
#CS 115 Sect. 012
#Date - 9/22/14

#Purpose: To develop a table of costs, based on the user's inputs of the starting and ending years and the formula.
#Preconditions: User inputs starting year, ending year, and step size for the table.
#Postconditions: Program outputs table with starting and ending years, as well as year values that coincide with the step size inputted by the user. Will also print the cost per gygabyte of hard drive storage based on year.

def main():     #Main Function
    
    #Heading/Title of program   
    print("Big Blue Hard Drive Storage Cost")  
    
    print("\n")  #Spacing Purposes    
    
    #Define variable for starting year based on user input
    StartingYear = int(input("Enter the starting year: "))
    #Define variable for ending year based on user input
    EndYear = int(input("Enter the ending year: "))
    #Ask user for step size of the table based on user input
    StepSize = int(input("What step size for the table? "))  
    
    print("\n")  #Spacing Purposes
    #Print title of cost table
    
    print("\t","Hard Drive Storage Costs Table")
    
    print("\n")  #Spacing Purposes
    
    #Print Start year so user is aware of their input
    print("Start Year = ",StartingYear)
    #Print End year so user is aware of their input
    print("End Year = ",EndYear)
    
    print("\n")  #Spacing Purposes
    
    #Print the categories of the table; in this case, Year and Cost Per Gigabyte
    print("Year", "\t", "\t", "Cost Per Gigabyte ($)")
    
    print("\n")  #Spacing Purposes
    
    #Loop for the range of years desired with step size given by user
    for i in range(StartingYear,(EndYear+1),StepSize):
        
        #Define variable containing the function for Moore's Law that will run the loop the specified number of times
        Func = round(10**((-0.2502)*(i - 1980) + 6.304),3)
        
        #Print statement containing the years adjusted to the user's specifications for the step table and also the cost per gigabyte for that specific year. Print all of these under the categories of the cost table
        print(i,"\t","\t","\t",Func)
        
main()  #End of program

#The first value that will display a cost of 0 would be 2019. In order to prove this, I set my starting year to 2000 and ending year to 2100; my step size to 4. When I saw that the value was between 2016 and 2020, I simply went back and made my interval from 2010 to 2021; with a step size of 1. Based on these inputs, the program first returns a value of zero at the year 2019.