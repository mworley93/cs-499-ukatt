#Evan Wilson
#Section 012
#evan.wilson@uky.edu
#Program 1

from math import *

def main():
    print("Big Blue Hard Drive Storage Cost")
    print(" ")
    syear = int(input("Enter the starting year: "))
    
    eyear = int(input("Enter the ending year: "))
    
    ss = int(input("What step size for the table? "))
    
    cost = 0
    year = 0
    output = 0
    print(" ")
    print("        Hard Drive Storage Costs Table")
    print(" ")
    
    print("Start Year:", syear)
    print("End Year:", eyear)
    print(" ")
    print("     Year     Cost Per Gigabyte ($)     ")
    print(" ")
    for i in range(syear, eyear, ss):
        cost = 10 ** (-0.2502 * (i-1980) + 6.304)
        print ("    ", i, end=" ") 
        output = (round(cost, 3))
        print ("   ", output)
main()
