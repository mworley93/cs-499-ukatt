#Robert Joseph Nguyen
#Robertjosephn@gmail.com
#Section 12
#CS 115 Program 1
#September 27 2014
#Purpose: Produce a table of costs, based on the user's inputs of the starting and ending years and the formula.
#Pre-conditions: User must input starting year, ending year and step size.

def main():
    # Print introductory message
    print("Big Blue Hard Drive Storage Cost \n")
    # Assign variables for the starting year, ending year, step size inputs.
    startyear= int(input("Enter the starting year:  "))
    endyear= int(input("Enter the ending year:  "))
    step= int(input("What step size for the table?  "))
    # Print table title
    print("\n        Hard Drive Storage Costs Table \n")
    # Print "start year =" 
    print("Start Year =", startyear)
    # Print "end year ="
    print("End Year = ", endyear, "\n")
    # Print "Year"  Print "Cost Per Gigabyte ($)"
    print("      Year             Cost Per Gigabyte ($) \n")
    # Loop for the range of years desired with step given by user
    for i in range(startyear,endyear+1):
        #Break loop when starting year is greater or equal to endyear + 1
        if(startyear >= endyear +1):
            break
         # calculate the cost per gigabyte for the year given by the loop
        cpg= float(10**(-0.2502*(startyear-1980) + 6.304))
        # Step 9. Print the year, Print Cost per Gigabyte ($)
        print("     ",startyear,"               ",   "%.3f" %cpg)
        startyear=startyear+step
              
main()

#BONUS
    # Offically, the cost will never hit exactly 0 but it becomes so small that its assumed to be 0. 2019 is the first year that produces ".000" but in 2015 it is smaller than a rounded up penny(.004), technically being 0.00.